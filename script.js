$(document).ready(function() {
	
	formInit ('http://university.netology.ru/api/currency');
	$('input').NumOrArrows();

	// Расчет по количеству валюты
	$('form').on('keyup', 'input', function() {
		var current = getFormCurrents();
		
		switch ($(this).context.id) {
			case 'issue':
				var newResult = convertMoney (this.value, current.fromVal, current.toVal);
				showResult (current.toRate, newResult);
				$("#result").val(newResult);
			break;
			case 'result':
				var newResult = convertMoney (this.value, current.toVal, current.fromVal);
				showResult (current.fromRate, newResult);
				$("#issue").val(newResult);
			break;
		}
	});	 
		
	// Расчет при новой валюте
	$('form').on('change', 'select', function(){
		$($(this).serializeArray()).each(function() {
			var current = getFormCurrents();
			
			switch (this.name){
				case 'from':
					var newResult = convertMoney (current.issueFrom, current.fromVal, current.toVal);
					showResult (current.toRate, newResult);
					$("#result").val(newResult);
				break;
				case 'to':
					var newResult = convertMoney (current.issueTo, current.toVal, current.fromVal);
					showResult (current.fromRate, newResult);
					$("#issue").val(newResult);
				break;
			}
		});
	});

});	
			
/*********************************
	Helpers
**********************************/

	// инициализация формы
	function formInit (apiUrl) {
		$.getJSON(apiUrl, function(data) {
			$('H1')
				.append($('<i/>', { text: data.length +' варианта' })
				.attr('data-role','info'));
			
		// Размещение опциональных настроек для выбора валюты
			$('select').append(function() {
				return data.map(function(item) {
						return $('<option/>')
							.attr({
								'value': (item.Value * item.Nominal), 
								'data-rate': item.CharCode })
							.html(item.Name +'-'+ item.CharCode);
					});
			}).get().join();
		
		// Выставление селекторов и данных по умолчанию
		setDefaultCurrent('#from', 'USD', 1);
		setDefaultCurrent('#to', 'EUR', 2);
		$("#issue").val(500);
		
		// Первичный расчет 
		var currentData = getFormCurrents();
		var newResult = convertMoney(currentData.issueFrom, currentData.fromVal, currentData.toVal);
		
		// Создание места для вывода результатов расчета
		$("#result").val(newResult);
		$('form').append( $("<div/>").addClass('result'));
		
		// Результат
		showResult(currentData.toRate, newResult);
		console.info('Сконвертирована валюта из %s в %s', currentData.fromRate, currentData.toRate);
	});
	}
	
	// Выставление селектора по умолчанию
	function setDefaultCurrent (setForm, setCurrent, setDefault) {
		$(setForm).find('option:eq('+ setDefault +')').prop('selected', true);
		
		$(setForm+' > option').each( function() {
			if ($(this).data('rate') == setCurrent) {
				$(setForm+' :nth-child('+ setDefault +')').prop('selected', false);
				$(this).prop('selected', true);
				return;
			}
		});
	}
	
	// Конвертация 
	function convertMoney (issueSet, convertFrom, convertTo) {
		var res = ( issueSet * convertFrom / convertTo );
			console.info('расчетное значение в валюте конвертации: %d', res);
		return res.toFixed(2);
	}
	
	// Получение данных из формы
	function getFormCurrents () {
		var currents = {};
			currents.fromVal		= $('#from').val();
			currents.fromRate		= $('#from option:selected').data('rate');
			currents.toVal 			= $('#to').val();
			currents.toRate 		= $('#to option:selected').data('rate');
			currents.issueFrom	= $("#issue").val();
			currents.issueTo		= $("#result").val();
		return currents;
	}

	// Отображение результата
	function showResult (exchange, sign) {
		$('.result').text('расчет в '+ exchange +' '+ sign );
	}
	
	// Проверка на символы
	jQuery.fn.NumOrArrows = function() {
		return this.each(function() {
			$(this).keydown(function(e) {
				var key = e.charCode || e.keyCode || 0;
				return (
					key == 8 || 
					key == 9 || 
					key == 46 || 
					(key >= 37 && key <= 40) || 
					(key >= 48 && key <= 57) || 
					(key >= 96 && key <= 105) 
				);
			});
		});
	}